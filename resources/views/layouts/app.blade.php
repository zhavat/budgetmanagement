<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sample App</title>

    <!-- Fonts -->
    <link href="/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <!-- {{-- <link href="{{ elixir('css/custom.css') }}" rel="stylesheet"> --}} -->

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
		#spinnerBackground {
            background: rgba(0,0,0,0.3);
            display: none;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999997;
        }
        #sidebar-menu{
            padding-top: 70px;
        }
    </style>
</head>
<body id="app-layout" class="nav-md">
<div class="container body">

        <div id="spinnerBackground" style="display:none;">
        </div>
        <div id="spinnerID" style="position:fixed;z-index:999999;left:50%;top:50%;"></div>
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            My App
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <!-- menu profile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="/images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Merhaba,</span>
                            <h2>Cevat Şirin</h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->
                    <br />
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="{{ url('/login') }}"><i class="fa fa-table"></i> Login</a></li>
                                <li><a href="{{ url('/register') }}"><i class="fa fa-institution"></i> Register</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->
                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small" style="height:45px;">
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="~/Content/images/img.png" alt="" /> Cevat
                                    <span class="fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="{{ url('/profile') }}"> Profil</a></li>
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i> Çıkış</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->
            <!-- page content -->
            @yield('content')
            <!-- /page content -->
            <!-- footer content -->
            <footer class="main-footer" style="z-index:999999">
                <div class="pull-right hidden-xs" style="margin-right:230px;">
                    <b>Version</b> 1.1.0
                </div>
                <!-- <strong>Copyright © 2017 <span class="logoAlgosoft" style=""><a href="http://www.algosoft.com.tr" target="_blank">Algosoft</a></span>.</strong> All rights reserved. -->
            </footer>
            <!-- /footer content -->
        </div>
    </div>
   

    <!-- JavaScripts -->
    <script src="/scripts/jquery-2.1.4.min.js"></script>
    <script src="/scripts/bootstrap.min.js"></script>
    <script src="/scripts/custom.js"></script>
    <!-- {{-- <script src="{{ elixir('') }}"></script> --}} -->
</body>
</html>
