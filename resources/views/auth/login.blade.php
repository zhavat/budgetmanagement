
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My App | Giriş yap</title>
    <!-- <link href="~/Content/bootstrap.css" rel="stylesheet" /> -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet" />
    <!-- <link href="~/Content/nprogress.css" rel="stylesheet" /> -->
    <link href="/css/custom.css" rel="stylesheet" />
    <!-- <link href="~/Content/css/jquery.alerts.css" rel="stylesheet" />
    <link href="~/Content/css/iCheck/skins/flat/green.css" rel="stylesheet" /> -->
</head>

<body class="login">
    <div id="spinnerID" style="position:absolute;left:50%;top:50%;"></div>
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>
        <div class="login_wrapper">
            <div class="animate form login_form x_panel">
                <section class="login_content">
                    <img src="/images/logo.png" width="100px" />
                    <h3 align="middle"><label style="color:dodgerblue;">My App</label></h3>
                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}
                        <h1>Giriş</h1>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input type="email" onkeypress="EnterEvent(event);" placeholder="E-posta" class="form-control" id="txtEmail" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input type="password" onkeypress="EnterEvent(event);" placeholder="Şifre" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox" style="float:left;margin-top:0px;">
                                    <label>
                                        <input type="checkbox" name="remember"> Beni hatırla
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success submit" style="width:100%;" id="btnLogin">
                                    <i class="fa fa-arrow-circle-o-right"></i> Giriş yap
                                </button>
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Şifreni mi unuttun?</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group" style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                            <div class="col-md-6 control" align="center">
                                <a href="{{ url('/language/tr') }}">Türkçe</a>
                            </div>
                            <div class="col-md-6 control" align="center">
                                <a href="{{ url('/language/en') }}">English</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <div class="clearfix"></div>
                            <br />
                            <br />
                            <br />
                            <br />
                            <div>
                                
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>
</html>

<script src="/scripts/jquery-2.1.4.min.js"></script>
<script src="/scripts/bootstrap.min.js"></script>
<script src="/scripts/custom.js"></script>
<script>
    $(function(){
        $('#txtEmail').focus();
    });
    function EnterEvent(e) {
        if (e.keyCode == 13) {
            $('#btnLogin').click();
        }
    }
</script>