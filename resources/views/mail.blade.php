<div id=":o8" class="ii gt adP adO"><div id=":sy" class="a3s aXjCH m14bdfb3ebd9c4fb2">
<div style="background-color:#dddddd;"><div class="adM">
</div><table border="0" cellpadding="0" cellspacing="0" style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin:20px;color:#6e5f5f;font-weight:normal;font-style:normal;line-height:20px;margin-left:auto;margin-right:auto" width="600">
<tbody>
<tr>
<td height="40"></td>
</tr>
<tr>
<td style="text-align:center">
<a href="http://butcem.com" style="text-decoration:none;border:0" target="_blank">
<img src="http://panel.butcem.com/Content/images/logo.png" width="60px">
</a>
</td>
</tr>
<tr>
<td height="40"></td>
</tr>
<tr>
<td>
<div style="background-color:#ffffff;border-radius:6px">
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td colspan="3" height="60"></td>
</tr>
<tr>
<td width="60"></td>
<td width="480">
<h1 style="font-size:18px;font-weight:bold;margin-bottom:40px;color:#6e5f5f">
<span class="il">BÜTÇEM</span>'e Gösterdiğiniz İlgi için Teşekkür Ederiz.

</h1>
<div style="font-size:15px;line-height:20px;color:#6e5f5f">
<p style="margin-bottom:20px">
<span class="il">BÜTÇEM</span> hesabınıza giriş yapmak ve hemen gelirlerinizi, giderlerinizi ve bütçe yönetiminizi başlamak için lütfen aşağıdaki "Onayla" butonuna tıklayarak aktivasyon işlemini gerçekleştiriniz.
</p>
<p style="margin-bottom:40px">
<a href="{{$url}}/{{$activation_part}}" style="display:inline-block;zoom:1;height:20px;line-height:20px;min-width:50px;padding:10px 15px;background:#109d14;color:#fdfefd;font-size:13px;outline:none;border:0;font-weight:bold;text-decoration:none;letter-spacing:0.5px;border-radius:6px" target="_blank"> Onayla </a>
</p>
<p style="margin-bottom:20px">
Firma hesabınıza istediğiniz yer ve zamanda
<a href="{{$url}}" style="color:#685f5d" target="_blank">Kullanıcı Girişi</a>
sayfasından kayıtlı e-posta adresiniz ve parolanızla giriş yapabilirsiniz. Bilgilerinizin güvenliği için parolanızı kimse ile paylaşmayın. Hesabınıza giriş yaptıktan sonra şifrenizi değiştirmeyi unutmayınız.
</p>
<p>
Her türlü soru ve önerilerinizi
<a href="mailto:destek@algosoft.com.tr" style="color:#685f5d" target="_blank">destek@butcem.com</a>
’ye yazabilirsiniz.
<br>
<br>
Sevgiler,
<br>
<b><span class="il">BÜTÇEM</span> Ekibi</b>
</p>

</div>
</td>
<td width="60"></td>
</tr>
<tr>
<td colspan="3" height="60"></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
<tr>
<td height="40"></td>
</tr>
<tr>
<td style="font-size:13px;color:#a19292;text-align:center">
<p>
<b>
<a href="http://www.algosoft.com.tr" style="color:#a19292;text-decoration:none" target="_blank"><span class="il">© 2017 Algosoft Ltd. Şti.</span></a>
</b>
<br>
<a href="mailto:destek@algosoft.com.tr" style="color:#a19292;text-decoration:none" target="_blank">destek@butcem.com</a>
— (0232) 988 10 19 (09:00–18:00)
<br>
</p>
</td>
</tr>
<tr>
<td height="40"></td>
</tr>
</tbody>
</table>
</div>