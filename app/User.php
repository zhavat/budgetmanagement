<?php

namespace App;

//use App\Task;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'activation_code', 'password_view'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'active', 'activation_code', 'password_view'
    ];

    /**
     * Get all of the tasks for the user.
     */
    // public function tasks()
    // {
    //     return $this->hasMany(Task::class);
    // }
}
