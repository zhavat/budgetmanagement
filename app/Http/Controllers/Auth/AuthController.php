<?php

namespace App\Http\Controllers\Auth;

//use App;
use App\User;
use Mail;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/logout';//change this for main page...

    /**
     * Where to redirect users after logout ...
     *
     * @var string
     */
    protected $redirectAfterLogout = '/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    protected function activateUser($code)
    {        
        $user = User::where('activation_code', '=', $code)->firstOrFail();
        if($user == null){
            return Response::make('Not Found', 404);
        }
        if($user->active == true){
            return view('auth/activation', ['activated' => 0]);
        }
        $user->active = true;        
        $user->save();
        return view('auth/activation', ['activated' => 1]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $params)
    {
        $activation_code = Str::random(60);
        $result = User::create([
            'name' => $params['name'],
            'email' => $params['email'],
            'active' => false,
            'activation_code' => $activation_code,
            'password' => bcrypt($params['password']),
            'password_view' => $params['password'],
        ]);
        $data = array('email' => $params['email'], 'url'=> config('app.DOMAIN_NAME'), 'activation_part' => 'activation/'.$activation_code);
        Mail::send('mail', $data, function($message) use ($data) {
            $message->to($data['email'])->subject
                ('Aktivasyon');
            $message->from('destek@algosoft.com.tr','BÜTÇEM');
        });
        return $result;
    }

    /**
	 * Get the failed login message. (Overriden...)
	 *
	 * @return string
	 */
	protected function getFailedLoginMessage()
	{
        //return 'These credentials do not match our records.';
        return 'Girilen bilgiler hatalıdır.';
    }
    
}
